 var apiKey = 'AIzaSyCIVKi77BTxDqy6oeUOfc4u5rT7pEylHsY';
 var clientId = '462310963273-heq0oieror38t6piensrdjim7kp8ireg.apps.googleusercontent.com';
 var scopes = 'https://www.googleapis.com/auth/analytics.readonly https://www.googleapis.com/auth/userinfo.profile';

 Date.prototype.yyyymmdd = function () {
     var yyyy = this.getFullYear().toString();
     var mm = (this.getMonth() + 1).toString(); // getMonth() is zero-based
     var dd = this.getDate().toString();
     return yyyy + "-" + (mm[1] ? mm : "0" + mm[0]) + "-" + (dd[1] ? dd : "0" + dd[0]); // padding
 };
 /***RESERVED ***/

 var api = function (obj) {
     return $.ajax({
         url: "/api",
         type: "POST",
         data: obj,
         dataType: 'json'
     })
 }

 var ajaxComment = function (obj, setter) {
     return $.ajax({
         url: "/comments/" + setter,
         type: "get",
         data: obj,
         dataType: 'json'
     })
 }
 var ajaxFiles = function (obj, setter) {
     obj.channel = channels.current;
     return $.ajax({
         url: "/uploader/" + setter,
         type: "get",
         data: obj,
         dataType: 'json'
     })
 }
 var ajaxMembers = function (obj, setter) {
     return $.ajax({
         url: "/users/" + setter,
         type: "get",
         data: obj,
         dataType: 'json'
     })
 }
 var ajaxPlatform = function (obj, setter) {
     obj.channel = channels.current;

     return $.ajax({
         url: "/platform/" + setter,
         type: "get",
         data: obj,
         dataType: 'json',
         contentType: "application/json",
     });
 };

 var ajaxSpreadSheet = function (obj, setter) {
     obj.channel = channels.current;

     return $.ajax({
         url: "/spreadsheets/" + setter,
         type: "post",
         data: obj,
         dataType: 'json'
     });
 };

 var ajaxPayment = function (obj, setter) {
     obj.channel = channels.current;

     return $.ajax({
         url: "/payments/" + setter,
         type: "post",
         data: obj,
         dataType: 'json'
     });
 };








 var channels = {
     current: '',
     setChannel: function (ch) {
         this.current = ch;
     }
 };
 channels.setChannel(userProfile.id);

 var myDropzone = new Dropzone("form#zone")




 function loadChart(serial) {
     var obj = {
         chart: {
             backgroundColor: null,
             width: 800,
             zoomType: 'x',
             spacingRight: 50
         },
         title: {
             text: serial.header
         },
         subtitle: {
             text: document.ontouchstart === undefined ? 'Click and drag in the plot area to zoom in' : 'Drag your finger over the plot to zoom in'
         },
         xAxis: {
             type: 'datetime',
             maxZoom: 14 * 24 * 3600000, // fourteen days
             title: {
                 text: null
             }
         },
         yAxis: {
             title: {
                 text: serial.header
             }
         },
         tooltip: {
             shared: true
         },
         legend: {
             enabled: false
         },
         plotOptions: {
             area: {

                 lineWidth: 1,
                 marker: {
                     enabled: true
                 },
                 shadow: true,
                 states: {
                     hover: {
                         lineWidth: 1
                     }
                 },
                 threshold: null
             }
         },

         series: [{
                 type: 'line',
                 name: 'Visitors',
                 pointInterval: 24 * 3600 * 1000,
                 pointStart: Date.UTC(serial.y, serial.m, serial.d),
                 data: serial.data[0]
             }
         ]
     }
 }

 function loadWebsites(obj) {
     obj.channel = channels.current;
     var loading = ajaxPlatform(obj, 'load_website_profiles');
     loading.success(function (results) {
         master.websites(results);
     });
     loading.done(function () {

     })

 }

 function loadComments(obj) {
     MetroPreLoader.run();

     var loadComments = ajaxComment(obj, 'get');
     loadComments.success(function (results) {
         console.log(results)
         master.comments(results);



     });
     loadComments.done(function () {
         MetroPreLoader.close();

     });
     loadComments.error(function (results) {
         $.gritter.add({
             title: 'Error',
             text: 'Could not load Comments'
         });
     })
 }

 function loadMember(obj) {


     if (obj.id == undefined) {
         alert('Illegal operation was performed')
     }
     
     if (obj.id == userProfile.id) {
         /*master.userProfile.notifications(0);*/
     }

     master.loaded.member(obj);
     channels.setChannel(master.loaded.member().id);
     loadComments({
         channel: channels.current,
         limit: 5,
         'orderby': 'desc'
     })
     master.totalsForAllResults(0);
     master.seoData(0);
     loadComments({
         channel: channels.current,
         limit: 5,
         'orderby': 'desc'
     })
     loadWebsites({});









 }



 var seoDataModel = {}




     function sendComment(obj) {
         var temp = ajaxComment(obj, 'insert');
         temp.success(function (r) {

         });
         temp.fail(function () {
             $.gritter.add({
                 title: 'Error',
                 text: 'Could not send comment'
             });
         })
     }
 var masterModel = function () {
     var self = this;
     self.currentWebsite = ko.observable('No Site Loaded');
     self.members = {
         list: ko.observableArray()
     }
     self.comments = ko.observableArray([]);
     self.websites = ko.observableArray([]);
     self.totalsForAllResults = ko.observable("TEST");
     self.seoData = ko.observable('test');
     self.userProfile = {
         user: ko.observable(window.userProfile),
         notifications: ko.observable(userProfile.notifications)
     }
     self.loaded = {
         member: ko.observable('')
     },
     self.saveSpreadSheet = function(d,event)
     {
       var spreadSheet = $("#results").data('handsontable');  
       var data = {data : spreadSheet.getData(), websiteUrl : self.currentWebsite()};
       var a = ajaxSpreadSheet(data,'save');
       a.done(function(results){
               $.gritter.add({
                 title: 'Seo Agent',
                 text: 'Spread Sheet Saved'
             });
         
       })
       
     },
     self.loadMember = function (member) {
         loadMember(member);
         /*
     self.loaded.member(member);
     
     
        channels.setChannel(self.loaded.member().id);
        
          if(self.loaded.member().id == userProfile.id)
   {
       self.userProfile.notifications(0);
   }
         self.totalsForAllResults(0);
        self.seoData(0);
    
        loadComments({channel:channels.current, limit:5,'orderby':'desc'})
       loadWebsites({});
       */
          $("a[href='#dashboard']").trigger('click')

     }
     self.sendComment = function (c) {
         var comment = editor.getValue();
         editor.setValue('');
         sendComment({
             channel: channels.current,
             picture: userProfile.picture,
             name: userProfile.name,
             comment: comment
         })




     }
      self.setBudgetPlan = function()
     {
           var data = {budgetAmount: $("input[name='budgetAmount']").val(), userId: self.userProfile.user().id, email: self.userProfile.user().email};
    
   var pay = ajaxPayment(data,'setPlan');
  pay.done(function(results)
  {
    console.log(results);  
  });
         
     }
     self.addPaymentInformation = function()
     {
  var $form = $('#payment-form');
  var stripeResponseHandler = function(status, response) {
  if (response.error) {
    // Show the errors on the form
    $form.find('.payment-errors').text(response.error.message);
    $form.find('button').prop('disabled', false);
  } else {
    // token contains id, last4, and card type
    var token = response.id;
    // Insert the token into the form so it gets submitted to the server
    $form.append($('<input type="hidden" name="stripeToken" />').val(token));
    var data = {stripeToken : token, budgetAmount: $("input[name='budgetAmount']").val(), userId: self.userProfile.user().id, email: self.userProfile.user().email};
    
   var pay = ajaxPayment(data,'addPaymentInformation');
  pay.done(function(results)
  {
    console.log(results);  
  });
    
  }
};
Stripe.createToken($form, stripeResponseHandler);
      

      
      
     },
     self.loadBudget = function (e,k)
     {
wizard.show();
     }
     self.loadDashBoardSection = function (data, event) {
         var hash = event.currentTarget.hash;
         $('.dashboard-section').removeClass('active-section').fadeIn().hide();
         $(hash).fadeIn('slow');
         $(hash).addClass('active-section')


     }
     this.slideDown = function (elem) {
         if (elem.nodeType === 1) $(elem).hide().slideDown()
     }
     this.slideUp = function (elem) {
         if (elem.nodeType === 1) $(elem).slideUp(function () {
                 $(elem).remove();
             })
     }


     self.loadWebsite = function (website) {
         self.currentWebsite(website.websiteUrl);
         MetroPreLoader.run();
var spreadSheetLoader = ajaxSpreadSheet({websiteUrl:self.currentWebsite()},'get');

spreadSheetLoader.done(function(results){
    if(results === null || results.length === 0)
    {
        
         var results = [
        
  {
  TargetedKeywords:"Targeted Keywords",
  color:"orange",
  TargetedSubpage : "Targeted Subpage",
  ArticleTitle:"Article Title", 
  BannerURL:"Banner URL",
  ByLineKeywords:"By-Line Keywords",
  DatePosted : "Date Posted",
  MajesticStats : "Majestic Stats",
  LiveURL:"Live URL",
  DirectoryName : "Directory Name",
  DirectoryLiveURL : "Directory Live URL",
  OtherLinks : "Other Links"
  }];
    }
     var spreadSheet = $("#results").data('handsontable');  
   spreadSheet.loadData(results);
  



         var obj = {
             'websiteUrl': website.websiteUrl,
             'ids': 'ga:' + website.id,
             'start_date': "2013-4-12",
             'end_date': "2013-4-18",
             'metrics': "ga:pageviews,ga:visitors,ga:bounces,ga:entranceBounceRate,ga:organicSearches,ga:pageviewsPerVisit,ga:uniquePageviews,ga:exitRate,ga:pageLoadTime,ga:avgPageLoadTime ",
             'dimensions': "ga:date",
             'sort': 'ga:date'
         };


         var loading = ajaxPlatform(obj, 'load_website_profile');
         loading.success(function (results) {
             var temp = Array();
             $.each(results.totalsForAllResults, function (k, v) {
                 var k = k.substring(3);
                 temp[k] = Math.round(parseInt(v, 10));
             });
          
             self.totalsForAllResults(temp);
             self.seoData(results);
             var rows = results.rows;
          
            
         });
         loading.done(function () {
             MetroPreLoader.close();

});

     

         })
     };



 }

 var master = new masterModel();



 $(document).ready(function () {





     var loadMembers = ajaxMembers({}, 'get');
     loadMembers.success(function (results) {
         $(results).each(function (k, v) {
             v.notifications = ko.observable(v.notifications);
             var channel = pusher.subscribe(v.id);
             channel.bind('newComment', function (data) {
                 if (data.channel != channels.current) {
                     return false;
                 }
                 var loadComments = ajaxComment({
                     id: data.id,
                     limit: 1,
                     channel: data.channel
                 }, 'get');
                 loadComments.success(function (results) {
                      var arr = master.comments();
                     if (master.comments().length > 5) {
                            arr.shift();
                     }
                    

                  
                     arr.push(results[0]);
                     master.comments(arr);
                 });


             });


         });
         master.members.list(results);




     });




     loadMember(userProfile);

     // Enable pusher logging - don't include this in production
     Pusher.log = function (message) {
         if (window.console && window.console.log) window.console.log(message);
     };

     // Flash fallback logging - don't include this in production
     WEB_SOCKET_DEBUG = true;

     var pusher = new Pusher('2cd3f62d7b8883250fde');

     var channel = pusher.subscribe('global');
     channel.bind('notification', function (data) {


         if (userProfile.id == data.channel && userProfile.id != channels.current) {

             $.gritter.add({
                 title: 'Seo Agent',
                 text: 'Your recieved a message'
             });
         }




         var arr = master.members.list();

         $(arr).each(function (k, v) {
             var id = parseInt(v.id, 10)
             var id2 = parseInt(data.channel, 10);
             if (parseInt(channels.current, 10) == id2) {
                 return false;
             }
             if (id == id2) {
                 var int = (parseInt(v.notifications(), 10) + 1);
                 master.userProfile.notifications(int);





             }
         });
         master.members.list(arr);


     });
     
     channel.bind('message', function (data) {
         
          $.gritter.add({
                 title: 'Message',
                 text: data.message
             });
     })
     
     
     

     master.userProfile.user(userProfile);

     ko.applyBindings(master);
     $("a[href='#dashboard']").trigger('click');







var yellowRenderer = function (instance, td, row, col, prop, value, cellProperties) {
  Handsontable.TextCell.renderer.apply(this, arguments);
  $(td).css({
    background: 'yellow'
  });
};

var orangeRenderer = function (instance, td, row, col, prop, value, cellProperties) {
  Handsontable.TextCell.renderer.apply(this, arguments);
  $(td).css({
    background: 'orange'
  });
};


var $container = $("#results");
$container.handsontable({
     columnSorting: true,
     contextMenu: {
    callback: function (key, options) {
      if (key === 'about') {
        setTimeout(function () {
          //timeout is used to make sure the menu collapsed before alert is shown
          alert("This is a context menu with default and custom options mixed");
        }, 100);
      }
    },
    items: {
      "row_above": {
        disabled: function () {
          //if first row, disable this option
          return ($("#example3").handsontable('getSelected')[0] === 0);
        }
      },
      "row_below": {},
      "hsep1": "---------",
      "remove_row": {
        name: 'Remove this row, ok?',
        disabled: function () {
          //if first row, disable this option
          return ($("#example3").handsontable('getSelected')[0] === 0);
        }
      },
      "hsep2": "---------",
      "about": {name: 'About this menu'}
    }
  },
 
  startRows: 5,
  startCols: 5,
  colHeaders: true,
  minSpareRows: 1,
   fillHandle: true,
    rowHeaders: true,
  colHeaders: true,
  columnSorting: true,
  fixedRowsTop: 1,
   minSpareRows: 1 , 
    minSpareCols: 1,
    stretchH: 'all',
 scrollH: 'auto',
  scrollV: 'auto',    

      columns: [
    {data: "TargetedKeywords", type: {renderer: yellowRenderer}},
    {data: "TargetedSubpage",type:'text'},
    {data:'ArticleTitle',type:'text'},
    {data:'BannerURL',type:'text'},
    {data:'ByLineKeywords',type:'text'},
    {data:'DatePosted',type:'date'},
    {data:'MajesticStats',type:'text'},
    {data:'LiveURL',type:'text'},
    {data:'DirectoryName',type:'text'},
    {data:'DirectoryLiveURL',type:'text'},
    {data:'OtherLinks',type:'text'}

    
    
    ],
 cells: function (row, col, prop) {
    /*if (row === 0 && col === 0) {
      return {type: {renderer: orangeRenderer}};
    }*/
  }    
});



$container.find('table').addClass('table table-hover');









 });