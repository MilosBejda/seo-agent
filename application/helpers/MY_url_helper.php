<?php
function base_js()
{
 return base_url()."assets/javascripts";
    
}
function base_css()
{
 return base_url()."assets/stylesheets";
    
}
function script($url)
{
    return '<script type="text/javascript" src="'.$url.'" ></script>';
}
function style($url)
{
    return '<link href="'.$url.'" media="screen" rel="stylesheet" type="text/css" />';
}
function base_img()
{
 return base_url()."assets/images";
    
}
?>