<?php
class budget extends CI_model
{
    public $table = 'budgets';
    
    function __construct()
    {
        parent::__construct();
    }
    public function get($data)
    {
        ///$data['select'] = isset($data['select']) ? $data['select'] : '*';  
        $data['limit']  = isset($data['limit']) ? $data['limit'] : null;
        $data['offset'] = isset($data['offset']) ? $data['offset'] : null;
        ////$this->db->select($data['select']);
        
        if (isset($data['order_by']))
            $this->db->order_by("id", $data['order_by']);
        
        $this->db->where($data['where']);
        if (isset($data['where2'])) {
            $this->db->where($data['where2']);
            
        }
        if (isset($data['or_where'])) {
            $this->db->or_where($data['or_where']);
            
        }
        
        
        return $this->db->get($this->table, $data['limit'], $data['offset']);
    }
    public function insert($data)
    {
        $this->db->insert($this->table, $data);
        return $this->db->insert_id();
    }
    public function update($data)
    {
        $this->db->update($this->table, $data, $data['where']);
    }
    public function delete($data)
    {
        $this->db->delete($this->table, $data['where']);
        
    }
    public function save($data)
    {
        if (isset($data) == false) {
            return false;
        }
        $arr = $this->get(array(
            'where' => array(
                'websiteUrl' => $data['websiteUrl']
            )
        ))->result_array();
        if (empty($arr)) {
            $this->db->insert($this->table, array(
                'data' => base64_encode(serialize($data['data'])),
                'websiteUrl' => $data['websiteUrl'],
                'created_on' => time()
            ));
        } else {
            $this->db->where('websiteUrl', $data['websiteUrl']);
            
            $this->db->update($this->table, array(
                'data' => base64_encode(serialize($data['data'])),
                'websiteUrl' => $data['websiteUrl'],
                'updated_on' => time()
            ));
        }
    }
}
