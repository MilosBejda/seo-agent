<?php
class file extends CI_Model{
    public $table = 'files';
 function __construct()
 {
        parent::__construct();
 }
public function get($data)
{
$data['select'] = isset($data['select']) ? $data['select'] : '*';  
$data['limit'] = isset($data['limit']) ? $data['limit'] : null;
$data['offset'] = isset($data['offset']) ? $data['offset'] : null;
$this->db->select($data['select']);
if(isset($data['where'])){
    
$this->db->where($data['where']);
}
return $this->db->get($this->table, $data['offset'],$data['limit']);
}

public function insert($data)
{
 $this->db->insert($this->table, $data);
   return $this->db->insert_id();

}
public function update($data,$where)
{
  
$this->db->update($this->table, $data, $where); 
}
public function delete($data)
{
$this->db->delete($this->table, $data['where']); 

}
}

