<?php
class user extends CI_Model{
    public $table = 'users';
 function __construct()
 {
        parent::__construct();
 }
public function get($data = null)
{
$data['select'] = isset($data['select']) ? $data['select'] : '*';  
$data['limit'] = isset($data['limit']) ? $data['limit'] : null;
$data['offset'] = isset($data['offset']) ? $data['offset'] : null;
$this->db->select($data['select']);
if(isset($data['where'])){
$this->db->where($data['where']);
}
return $this->db->get($this->table, $data['offset'],$data['limit']);
}

public function insert($data)
{
 $this->db->insert($this->table, $data);
 return $this->db->insert_id();
}
public function update($data,$where)
{
  
$this->db->update($this->table, $data, $where); 
}
public function increment_notification($data)
{
    $this->db->where('id', $data['id']);
$this->db->set('notifications', 'notifications+1', FALSE);
$this->db->update($this->table);
}

public function delete($data)
{
$this->db->delete($this->table, $data['where']); 

}
}

