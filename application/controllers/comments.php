<?php
class comments extends MASTER_Controller /**MASTER located in the core**/
{
     function __construct()
     {
         parent::__construct();
            $this->load->model('message');
            $this->load->helper('url');
    }
public function insert()
{
  //  $socketId = $_GET['socketId'];

$data['channel'] =   $this->input->get('channel');
$data['message'] =   $this->input->get('comment');
$data['name'] =      $this->input->get('name');
$data['thumbnail'] = $this->input->get('picture');
$data['created_on'] = time();

$id = $this->message->insert($data);

$data = array('id' => $id,'channel'=>$channel);
$this->pusher->trigger($channel, 'newComment', $data);
$this->pusher->trigger('global', 'notification', $data);

}
public function get()
{
   
$data['where']['channel'] = $this->input->get('channel');
$id      = $this->input->get('id');
$data['limit'] = $this->input->get('limit');
$data['offset']  = $this->input->get('offset');
$data['order_by'] = isset($_REQUEST['orderby']) ? $_REQUEST['orderby'] :  'desc';


if(!empty($id))
{
$data['where']['id'] = $id;
}


$messages =  array_reverse($this->message->get($data)->result_array());

echo json_encode($messages);
die();
}


}