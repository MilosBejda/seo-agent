<?php
class Uploader extends MASTER_Controller{
function __construct()
{
    parent::__construct();
    $this->load->model('file');
}
function index()
{
 $this->load->helper(array('form', 'url'));

$config['upload_path'] = './files/';
    	$config['allowed_types'] = 'gif|jpg|png';
		$config['max_size']	= '100';
		$config['max_width']  = '1024';
		$config['max_height']  = '768';
        $config['file_name'] = substr(md5(time()),15);

		$this->load->library('upload', $config);
$ch = $_REQUEST['channel'];

		if ( ! $this->upload->do_upload('file'))
		{
			$error = array('error' => $this->upload->display_errors());

		print_r($error);
		}
		else
		{
			$data = $this->upload->data();
            
            $insert['raw_name'] = $data['raw_name'];
             $insert['file_size'] = $data['file_size'];
              $insert['orig_name'] = $data['orig_name'];
            $insert['file_ext'] = $data['file_ext'];  
            $insert['unix'] = time();
            $insert['channel'] = $ch;
            $id =  $this->file->insert($insert);
            
              $this->pusher->trigger($ch, 'fileUpload', array('id' => $id));

		}
}

function get()
{
$this->load->model('file');    
$channel = $_REQUEST['channel'];
$id = isset($_REQUEST['id']) ? $_REQUEST['id'] : null;

$limit = isset($_REQUEST['limit']) ? $_REQUEST['limit'] :  null;
$offset = isset($_REQUEST['offset']) ? $_REQUEST['offset'] :  null;
$order_by = isset($_REQUEST['orderby']) ? $_REQUEST['orderby'] :  'desc';
$data['where']['channel'] = $channel;

if(isset($id))
$data['where']['id'] = $id;


$data['order_by'] = $order_by;
$data['offset'] = $offset;
$data['limit'] = $limit;
$messages =  $this->file->get($data)->result_array();
echo json_encode($messages);
die();    
}


}
