<?php
require(APPPATH . 'libraries/seostats/seostats.php');

require(APPPATH . 'libraries/google_api/Google_Client.php');
require(APPPATH . 'libraries/google_api/contrib/Google_AnalyticsService.php');
require(APPPATH . 'libraries/google_api/contrib/Google_Oauth2Service.php');


class Platform extends MASTER_Controller
{
    public $client;
    public $refreshToken;
    private $channel;
    
    public function __construct()
    {
        parent::__construct();
        $this->load->helper(array('cookie','url'));
        $this->load->model('user');        
        $this->client = new Google_Client();
        $this->client->setApplicationName("Google Analytics PHP Starter Application");
        $this->client->setClientId('462310963273-heq0oieror38t6piensrdjim7kp8ireg@developer.gserviceaccount.com');
        $this->client->setClientSecret('6N3TmZ-exVWP4yVeOIUNmzSM');
        $this->client->setRedirectUri('http://seoagent.wooestate.com/platform');
        $this->client->setDeveloperKey('AIzaSyCIVKi77BTxDqy6oeUOfc4u5rT7pEylHsY');
        $this->client->setScopes(array(
            'https://www.googleapis.com/auth/plus.me',
            'https://www.googleapis.com/auth/analytics.readonly',
            'https://www.googleapis.com/auth/userinfo.profile',
            'https://www.googleapis.com/auth/userinfo.email'
        ));
        $this->client->setAccessType('offline');
        
        
        $this->refreshToken = $this->input->cookie('refresh', TRUE);
        if (isset($this->refreshToken) && !empty($this->refreshToken)) {
            $this->client->refreshToken($this->refreshToken);
        }
        
        
        if (isset($_GET['code'])) {
            $this->client->authenticate();
            $_SESSION['token'] = $this->client->getAccessToken();
            $redirect          = 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['PHP_SELF'];
            header('Location: ' . filter_var($redirect, FILTER_SANITIZE_URL));
        }
        
        if (isset($_SESSION['token'])) {
            $this->client->setAccessToken($_SESSION['token']);
        }
        
        $json = json_decode($this->client->getAccessToken());
        if (isset($json->refresh_token)) {
            
            
            
            $cookie = array(
                'name' => 'refresh',
                'value' => $json->refresh_token,
                'expire' => '86500',
                'path' => '/',
                'prefix' => '',
                'secure' => FALSE
            );
            
            $this->input->set_cookie($cookie);
            
            
        }
    
        $this->channel = $this->input->get('channel');
         if(!empty($this->channel))
        {
            
          $user =  array_shift($this->user->get(array('where'=>array('id'=>$this->channel)))->result_array());
          $this->client->refreshToken($user['refreshToken']);
        }
        
    }

    public function load_website_profiles()
    {
        $this->load->model(array('user','website'));
    
   
        
        $service = new Google_AnalyticsService($this->client);
         $props = $service->management_profiles->listManagementProfiles("~all","~all");
         
         foreach($props['items'] as $item)
            {
            $data['id'] = $item['id'];
            $data['websiteUrl'] = $item['websiteUrl'];
            $data['accountId'] = $item['accountId'];
            $data['serial'] = base64_encode(serialize($item));
            $data['username'] = $props['username'];
            $arr = array_shift($this->website->get(array('where'=>array('id'=>$data['id'],'accountId'=>$data['accountId'])))->result_array());
            if(count($arr) == 0)
            {
            $this->website->insert($data);
            }
        }
        
        echo json_encode($props['items']);
        die();
    }
    public function load_user_profiles()
    {
        $this->load->model('user');
       $arr = $this->user->get()->result_array();
       echo json_encode($arr);
        
    }
    public function load_website_profile()
    {
           
                $service = new Google_AnalyticsService($this->client);

        $id = $this->input->get('ids');
        $start = $this->input->get('start_date');
        $end = $this->input->get('end_date');
        $metrics = $this->input->get('metrics');
        $dimensions = $this->input->get('dimensions');
        $sort = $this->input->get('sort');
        $websiteUrl = trim($this->input->get('websiteUrl'));

$seoStats = new SEOstats($websiteUrl);

$start = date("Y-m-d", strtotime($start));
$end = date("Y-m-d", strtotime($end));


 try {
        $data = $service->data_ga->get($id, $start, $end, $metrics, array('dimensions'=>$dimensions, 'sort' => $sort, 'max-results' => 25));
    }
    catch (apiServiceException $e) {
        echo $e->getCode();
        print_r($data);
    }
$data['googlePageRank'] = ($seoStats->Google()->getPageRank() ? $seoStats->Google()->getPageRank() : '0');
$data['googlePageSpeedScore'] = (is_int($seoStats->Google()->getPagespeedScore()) ? $seoStats->Google()->getPagespeedScore() : '0');
$data['alexaGlobalPageRank'] = (is_int($seoStats->Alexa()->getGlobalRank()) ? $seoStats->Alexa()->getGlobalRank() : '0');
$data['alexaCountryPageRank'] = (is_int($seoStats->Alexa()->getCountryRank()) ? $seoStats->Alexa()->getCountryRank() : '0');
$data['alexaBackLinkCount'] = (is_int($seoStats->Alexa()->getBacklinkCount()) ? $seoStats->Alexa()->getBacklinkCount() : '0');
$data['websiteUrl'] = $websiteUrl;


   echo json_encode($data);
    }
    
    
    
    
    public function index()
    {
        
        
        $service = new Google_AnalyticsService($this->client);
        
        if ($this->client->getAccessToken()) {
              $user =      $this->login();
    
            // $props = $service->management_webproperties->listManagementWebproperties("~all");
            // $props = $service->management_profiles->listManagementProfiles("~all","~all");
            
            //  print "<h1>Web Properties</h1><pre>" . print_r($props, true) . "</pre>";
            /*foreach($props['items'] as $item)
            {
            $data['wid'] = $item['id'];
            $data['websiteUrl'] = $item['websiteUrl'];
            $data['accountId'] = $item['accountId'];
            $data['serial'] = base64_encode(serialize($item));
            $data['username'] = $props['username'];
            $arr = array_shift($this->website->get(array('where'=>array('wid'=>$data['wid'],'accountId'=>$data['accountId'])))->result_array());
            if(count($arr) == 0)
            {
            $this->website->insert($data);
            }
            
            $dimensions = array('visitCount');
            $metrics    = array('visitors');
            
            
            
            $r =    $service->data_ga->get(
            'ga:' . $item['id'],
            '2013-03-03',
            '2013-03-03',
            'ga:visits');
            echo "<pre>";
            print_r($r);
            echo "</pre>";
            
            }
            
            */
            /*
            $accounts = $service->management_accounts->listManagementAccounts();
            print "<h1>Accounts</h1><pre>" . print_r($accounts, true) . "</pre>";
            
            $segments = $service->management_segments->listManagementSegments();
            print "<h1>Segments</h1><pre>" . print_r($segments, true) . "</pre>";
            
            $goals = $service->management_goals->listManagementGoals("~all", "~all", "~all");
            print "<h1>Segments</h1><pre>" . print_r($goals, true) . "</pre>";
            */
            $_SESSION['token'] = $this->client->getAccessToken();
            $_SESSION['user'] = $user;
            $_SESSION['loggedin'] = true;
               $this->pusher->trigger('global', 'message', array('message'=>'SEO Agent '.$user['name'].' has loggedin'));    
              $this->twig->display('pages/dashboard.html',array('user'=>$user));
              
    
        } else {
            $authUrl = $this->client->createAuthUrl();
                      $this->twig->display('pages/login.html',array('loginUrl'=>$authUrl));
    
        }
        
        
        
        
        
        
        
        
    }
    private function login()
    {
        $this->load->model('user');
        $oauth2               = new Google_Oauth2Service($this->client);
        $user                 = $oauth2->userinfo->get();
        $arr                  = array_shift($this->user->get(array(
            'where' => array(
                'id' => $user['id']
            )
        ))->result_array());
        $user['refreshToken'] = isset($this->refreshToken) ? $this->refreshToken : '';
        if (isset($arr['id'])) {
            $this->user->update($user, array(
                'id' => $user['id']
            ));
        } else {
          $id =  $this->user->insert($user);
            $arr                  = array_shift($this->user->get(array(
            'where' => array(
                'id' => $user['id']
            )
        ))->result_array());
        }
        
        return $arr;
        
        // $this->twig->display('pages/login.html');    
        
    }
    
    
    
    public function logout()
    {
        $this->twig->display('pages/logout.html');
        
    }
    public function callback()
    {
        
        
        
    }
    function getFirstprofileId(&$analytics)
    {
        $accounts = $analytics->management_accounts->listManagementAccounts();
        
        if (count($accounts->getItems()) > 0) {
            $items          = $accounts->getItems();
            $firstAccountId = $items[0]->getId();
            
            $webproperties = $analytics->management_webproperties->listManagementWebproperties($firstAccountId);
            
            if (count($webproperties->getItems()) > 0) {
                $items              = $webproperties->getItems();
                $firstWebpropertyId = $items[0]->getId();
                
                $profiles = $analytics->management_profiles->listManagementProfiles($firstAccountId, $firstWebpropertyId);
                
                if (count($profiles->getItems()) > 0) {
                    $items = $profiles->getItems();
                    return $items[0]->getId();
                    
                } else {
                    throw new Exception('No profiles found for this user.');
                }
            } else {
                throw new Exception('No webproperties found for this user.');
            }
        } else {
            throw new Exception('No accounts found for this user.');
        }
    }
    
}
?>
