<?php
require(APPPATH.'libraries/REST_Controller.php');  
require(APPPATH.'libraries/seostats/seostats.php');  
require(APPPATH.'libraries/Pusher.php');  

class api extends REST_Controller {

public function __construct()
{
    parent::__construct();
}
public function url()
{
         $data = $this->get('url');

$SEOstats = new SEOstats($data);
  $arr['googlePageRank'] = $SEOstats->Google()->getPageRank();
  $arr['alexaBacklinks'] = $SEOstats->Alexa()->getBacklinkCount();
  $arr['googleBacklinks'] = $SEOstats->Google()->getBacklinksTotal();
  $arr['alexaGlobalRank'] = $SEOstats->Alexa()->getGlobalRank();
  $arr['alexaCountryRank'] = $SEOstats->Alexa()->getCountryRank(); 
  $arr['alexaQuarterRank'] = $SEOstats->Alexa()->getQuarterRank(); 
  $arr['alexaMonthRank'] = $SEOstats->Alexa()->getMonthRank(); 
  array_walk($arr, function(&$value, $key) use(&$arr) { 
$arr[$key] = is_int($arr[$key]) ? $arr[$key] : 0;
}); 

 
 
        $this->response($arr);
        die();
}
public function push()
{
$app_key = '2cd3f62d7b8883250fde';
$app_secret = 'c04d916aeae875cad936';
$app_id = '41900';
$pusher = new Pusher($app_key, $app_secret, $app_id);
$data = array('message' => 'This is an HTML5 Realtime Push Notification!');
$pusher->trigger('my_notifications', 'notification', $data);
}
}

