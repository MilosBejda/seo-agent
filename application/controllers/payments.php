<?php
class payments extends CI_Controller
{
     function __construct()
     {
         parent::__construct();
            $this->load->model('payment');
            $this->load->library('stripe');
            $this->load->helper(array('url','conversions'));
            $this->load->model('user');
            

    }
    private function getUser($userId)
    {
        return   array_shift($this->user->get(array('where'=>array('id'=>$userId)))->result_array());
    }

    private function getPaymentInformation($user)
    {

        $paymentInformation = unserialize(base64_decode($user['paymentInformation']));
        return $paymentInformation;
    }
    function addPaymentInformation()
    {
        $token = $this->input->post('stripeToken');
        $userId = $this->input->post('userId');
        $email = $this->input->post('email');

    $user = $this->getUser($userId);
    
    $customerInformation = $user['paymentInformation'];
    
        $customer = json_decode($this->stripe->customer_create($token, $user['email'], $user['email']));
        if(isset($customer->error))
        {
            print_r($customer->error);
            die();
        }
        $t['customerInformation'] =  base64_encode(serialize($customer));
        $this->user->update($t,array('id'=>$userId));
    
    }
    function setPlan()
    {
        $userId = $this->input->post('userId'); 
        $budgetAmount = $this->input->post('budgetAmount');     
    $budgetAmount = dollarToCent($budgetAmount);
    
    $user = $this->getUser($userId);
    $paymentInformation = $this->getPaymentInformation($user);
    $this->stripe->plan_create($user['id'],$budgetAmount,$user['name'],'month');
    $this->stripe->customer_subscribe($paymentInformation->id, $user['id']);

     
    
    }
    function addCharge()
    {
        $userId = $this->input->post('userId');
         
        $user = $this->getUser($userId);
        $r = $this->stripe->charge_customer($due, $customer->id,'SEO Agent DUE');
    }
    
    
    


}