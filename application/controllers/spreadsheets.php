<?php
class spreadsheets extends MASTER_Controller
{
 function __construct()
 {
        parent::__construct();
        $this->load->model('spreadSheet');
        $this->load->helper('url');


 } 
 function save()
 {
     $data['data'] = $_REQUEST['data'];
     print_r($data);
     
     $data['websiteUrl'] = $this->input->post('websiteUrl');
     $this->spreadSheet->save($data);
 }
 function ser($data)
{
    return unserialize(base64_decode($data['data']));
}
  function get()
 {
     $data['where']['websiteUrl'] = $_REQUEST['websiteUrl'];
     $arr = $this->spreadSheet->get($data)->result_array();
     $a = array_map(array($this,'ser'), $arr); 
     if(empty($a) || isset($a[0]) == false)
     {
         echo json_encode(array());
         die();
     }
     
     echo  json_encode($a[0]);
     
 }
}