<?php

/* /partials/sections/reports.html */
class __TwigTemplate_5e91271faff671f3e292389d5da16f04 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"files\">
<h1>Reports</h1>
<form id=\"zone\" class=\"dropzone\" action=\"/uploader/\" method=\"post\" enctype=\"multipart/form-data\">
  <input type=\"file\" name=\"file\" />
</form>
    
</div>
";
    }

    public function getTemplateName()
    {
        return "/partials/sections/reports.html";
    }

    public function getDebugInfo()
    {
        return array (  62 => 28,  60 => 27,  52 => 21,  50 => 20,  42 => 15,  37 => 12,  35 => 11,  33 => 10,  25 => 4,  23 => 3,  19 => 1,  187 => 88,  184 => 87,  179 => 83,  176 => 82,  164 => 103,  154 => 97,  148 => 94,  143 => 92,  139 => 90,  137 => 87,  133 => 85,  131 => 82,  126 => 79,  124 => 78,  103 => 60,  98 => 58,  85 => 48,  79 => 45,  65 => 34,  48 => 20,  44 => 16,  40 => 18,  21 => 1,);
    }
}
