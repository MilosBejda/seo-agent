<?php

/* /partials/sections/progress.html */
class __TwigTemplate_468a0b8311955942aae05c240913565c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"box\">
  <div class=\"box-header\">
    <div class=\"title\">SEO SpreadSheet</div>
    <ul class=\"box-toolbar\">
      <li class=\"toolbar-link\">
        <a href=\"#\" data-toggle=\"dropdown\"><i class=\"icon-cog\"></i></a>
        <ul class=\"dropdown-menu\">
          <li><a href=\"#\" data-bind=\"click: saveSpreadSheet\"><i class=\"icon-plus-sign\"></i> Save</a></li>
          <li><a href=\"#\"><i class=\"icon-remove-sign\"></i> Remove</a></li>
          <li><a href=\"#\"><i class=\"icon-pencil\"></i> Edit</a></li>
        </ul>
      </li>
    </ul>
  </div>


 <div id=\"results\" style=\"overflow: auto\"></div>


  
</div>";
    }

    public function getTemplateName()
    {
        return "/partials/sections/progress.html";
    }

    public function getDebugInfo()
    {
        return array (  62 => 28,  60 => 27,  52 => 21,  50 => 20,  42 => 15,  37 => 12,  35 => 11,  33 => 10,  25 => 4,  23 => 3,  19 => 1,  187 => 88,  184 => 87,  179 => 83,  176 => 82,  164 => 103,  154 => 97,  148 => 94,  143 => 92,  139 => 90,  137 => 87,  133 => 85,  131 => 82,  126 => 79,  124 => 78,  103 => 60,  98 => 58,  85 => 48,  79 => 45,  65 => 34,  48 => 20,  44 => 16,  40 => 18,  21 => 1,);
    }
}
