<?php

/* partials/titlebar.html */
class __TwigTemplate_21c1778389903aff55cae0fe1d7001d6 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo " <div class=\"area-top clearfix\" id=\"titleBar\">
        <div class=\"pull-left header\">
          <h3 class=\"title\" data-bind=\"text : dashboard.sectionName\">
            <i class=\"icon-dashboard\"></i>
            
          </h3>
          <h5>
          <div data-bind=\"text: loadedWebsiteProfile\"></div> 
          </h5>
       
        </div>
              <ul class=\"inline pull-left sparkline-box\">

          <li class=\"sparkline-row\">
            <h4 class=\"blue\"><span>Alexa Rank</span> </h4>
          </li>

          <li class=\"sparkline-row\">
            <h4 class=\"green\"><span>Google Page Rank</span></h4>
          </li>


        </ul>
    
        <ul class=\"inline pull-right sparkline-box\">


          <li class=\"sparkline-row\">
            <h4 class=\"blue\"><span>Bounce</span><div  data-bind=\"text: seoData.bounces\"></div> </h4>
          </li>

     
        </ul>
        
      </div>
      
";
    }

    public function getTemplateName()
    {
        return "partials/titlebar.html";
    }

    public function getDebugInfo()
    {
        return array (  19 => 1,);
    }
}
