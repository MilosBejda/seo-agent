<?php

/* /layouts/platform.html */
class __TwigTemplate_68e748e19c8bf432b2c42dfe961459ca extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'sidebar' => array($this, 'block_sidebar'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!doctype html>
<html>
<head>
  ";
        // line 4
        $this->env->loadTemplate("resources/meta/meta.html")->display($context);
        // line 5
        echo "  <title>SEO Agent Pro</title>
  ";
        // line 6
        $this->env->loadTemplate("resources/stylesheets/header-stylesheets.html")->display($context);
        // line 7
        echo "  ";
        $this->env->loadTemplate("resources/javascripts/header-javascripts.html")->display($context);
        // line 8
        echo "</head>
<body>
";
        // line 10
        $this->env->loadTemplate("resources/javascripts/body-javascripts.html")->display($context);
        // line 11
        $this->env->loadTemplate("navigations/default-navigation.html")->display($context);
        // line 12
        $this->displayBlock('sidebar', $context, $blocks);
        // line 14
        echo "<div class=\"main-content\">
";
        // line 15
        $this->displayBlock('content', $context, $blocks);
        // line 17
        echo "</div>
";
        // line 18
        $this->env->loadTemplate("resources/javascripts/footer-javascripts.html")->display($context);
        // line 19
        echo "</body>
</html>
";
    }

    // line 12
    public function block_sidebar($context, array $blocks = array())
    {
    }

    // line 15
    public function block_content($context, array $blocks = array())
    {
    }

    public function getTemplateName()
    {
        return "/layouts/platform.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  67 => 15,  62 => 12,  56 => 19,  54 => 18,  51 => 17,  49 => 15,  46 => 14,  44 => 12,  42 => 11,  40 => 10,  36 => 8,  33 => 7,  31 => 6,  28 => 5,  26 => 4,  21 => 1,);
    }
}
