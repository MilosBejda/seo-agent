<?php

/* /partials/sections/bootstrap.html */
class __TwigTemplate_65ca0469b826e6d85e19c3a42de5c2e4 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"container-fluid\">
<div class=\"row-fluid\">
";
        // line 3
        $this->env->loadTemplate("/partials/breadcrumbs.html")->display($context);
        // line 4
        echo "</div>
</div>
<div class=\"container-fluid padded\">
<div id=\"dashboard\" class=\"row-fluid dashboard-section\">
";
        // line 8
        $this->env->loadTemplate("/partials/chat-timeline.html")->display($context);
        // line 9
        $this->env->loadTemplate("/partials/chat.html")->display($context);
        // line 10
        echo "</div> 
<div  id=\"progress\" class=\"row-fluid dashboard-section\">
";
        // line 12
        $this->env->loadTemplate("/partials/sections/progress.html")->display($context);
        // line 13
        echo "</div>
<div  id=\"reports\" class=\"row-fluid dashboard-section\">
";
        // line 15
        $this->env->loadTemplate("/partials/sections/reports.html")->display($context);
        // line 16
        echo "</div>
<div  id=\"administration\" class=\"row-fluid dashboard-section\">
</div>
</div> 
";
        // line 20
        $this->env->loadTemplate("/partials/paymentModal.html")->display($context);
        // line 21
        echo "
";
    }

    public function getTemplateName()
    {
        return "/partials/sections/bootstrap.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  55 => 21,  53 => 20,  47 => 16,  45 => 15,  35 => 10,  33 => 9,  31 => 8,  25 => 4,  23 => 3,  19 => 1,  41 => 13,  39 => 12,  36 => 5,  32 => 3,  29 => 2,);
    }
}
