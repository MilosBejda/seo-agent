<?php

/* /sidebars/default-sidebar.html */
class __TwigTemplate_997014f7b671a33a686157cc1b053b84 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "
<div class=\"sidebar-background\">
  <div class=\"primary-sidebar-background\"></div>
</div>

<div id=\"sidebar\" class=\"primary-sidebar\">

  <!-- Main nav -->
  <ul class=\"nav nav-collapse collapse nav-collapse-primary\">


            <li class=\"active\">
              <span class=\"glow\"></span>
                       <a href=\"#dashboard\" data-bind=\"click: loadDashBoardSection\">

                  <i class=\"icon-dashboard icon-2x\"></i>
                  <span>Dashboard</span>

              </a>
            </li>


              <!-- ko if: (seoData().websiteUrl != undefined) -->

  <li class=\"\">
              <span class=\"glow\"></span>
              <a href=\"#progress\" data-bind=\"click: loadDashBoardSection\">
                  <i class=\"icon-bar-chart icon-2x\"></i>
                  <span>Progress</span>
              </a>
            </li>

            <li class=\"\">
              <span class=\"glow\"></span>
              <a href=\"#reports\" data-bind=\"click: loadDashBoardSection\">
                  <i class=\"icon-edit icon-2x\"></i>
                  <span>Reports</span>
              </a>
            </li>

              <!-- /ko -->

        <li class=\"\">
              <span class=\"glow\"></span>
              <a   data-toggle=\"modal\" href=\"#static\">
                  <i class=\"icon-plus-sign-alt icon-2x\"></i>
                  <span>Budget</span>
              </a>
            </li>

        

          
        

    

        

            <li class=\"dark-nav \">

              <span class=\"glow\"></span>

              

              <a class=\"accordion-toggle collapsed \" data-toggle=\"collapse\" href=\"#wW7abHF3rv\">
                  <i class=\"icon-beaker icon-2x\"></i>
                    <span>
                Administrator
                      <i class=\"icon-caret-down\"></i>
                    </span>

              </a>

              <ul id=\"wW7abHF3rv\" class=\"collapse \">
                
                    <li class=\"\">
                      <a data-bind=\"click: loadDashBoardSection\">
                          <i class=\"icon-hand-up\"></i> My Reports
                      </a>
                    </li>
                
                    <li class=\"\">
                      <a data-bind=\"click: loadDashBoardSection\">
                          <i class=\"icon-beaker\"></i> Progress
                      </a>
                    </li>
                
                    <li class=\"\">
                      <a data-bind=\"click: loadDashBoardSection\">
                          <i class=\"icon-info-sign\"></i> Users
                      </a>
                    </li>
                
                    <li class=\"\">
                      <a data-bind=\"click: loadDashBoardSection\">
                          <i class=\"icon-th-large\"></i> Task Lists
                      </a>
                    </li>
                
                    <li class=\"\">
                      <a data-bind=\"click: loadDashBoardSection\">
                          <i class=\"icon-table\"></i> Messages
                      </a>
                    </li>
                
                    <li class=\"\">
                      <a data-bind=\"click: loadDashBoardSection\">
                          <i class=\"icon-plus-sign-alt\"></i> Database
                      </a>
                    </li>
                
              </ul>

            </li>

        

    

        


  </ul>

  <div class=\"hidden-tablet hidden-phone\">
  
    <div class=\"text-center\" style=\"margin-top: 60px\">
      <div class=\"easy-pie-chart-percent\" style=\"display: inline-block\" data-percent=\"89\"><span>89%</span></div>
      <div style=\"padding-top: 20px\"><b>Monthly Progress</b></div>
    </div>

    <hr class=\"divider\" style=\"margin-top: 60px\">

    <div class=\"sparkline-box side\">

      <div class=\"sparkline-row\">
        <h4 class=\"gray\"><span>Total Budget</span> 847</h4>
        <div class=\"sparkline big\" data-color=\"gray\"><!--8,5,6,14,4,11,6,29,26,29,8,7--></div>
      </div>

      <hr class=\"divider\">
      <div class=\"sparkline-row\">
        <h4 class=\"dark-green\"><span>Link Building</span> \$43.330</h4>
        <div class=\"sparkline big\" data-color=\"darkGreen\"><!--12,22,28,21,20,23,5,20,22,24,23,7--></div>
      </div>

      <hr class=\"divider\">
      <div class=\"sparkline-row\">
        <h4 class=\"blue\"><span>Content Writing</span> 223</h4>
        <div class=\"sparkline big\" data-color=\"blue\"><!--3,19,5,24,11,8,8,29,5,14,26,10--></div>
      </div>

      <hr class=\"divider\">
       <div class=\"sparkline-row\">
        <h4 class=\"blue\"><span>Directory Submissions</span> 223</h4>
        <div class=\"sparkline big\" data-color=\"blue\"><!--3,19,5,24,11,8,8,29,5,14,26,10--></div>
      </div>
      
      <hr class=\"divider\">
      
      <div class=\"sparkline-row\">
        <h4 class=\"blue\"><span>Site Edits</span> 223</h4>
        <div class=\"sparkline big\" data-color=\"blue\"><!--3,19,5,24,11,8,8,29,5,14,26,10--></div>
      </div>
      <hr class=\"divider\">
    </div>
  </div>
 </div>
";
    }

    public function getTemplateName()
    {
        return "/sidebars/default-sidebar.html";
    }

    public function getDebugInfo()
    {
        return array (  19 => 1,);
    }
}
