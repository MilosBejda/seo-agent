<?php

/* pages/dashboard.html */
class __TwigTemplate_9923163a6719dcd22f262a0cdc0e4001 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("/layouts/platform.html");

        $this->blocks = array(
            'sidebar' => array($this, 'block_sidebar'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "/layouts/platform.html";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_sidebar($context, array $blocks = array())
    {
        // line 3
        $this->env->loadTemplate("/sidebars/default-sidebar.html")->display($context);
    }

    // line 5
    public function block_content($context, array $blocks = array())
    {
        // line 6
        $this->env->loadTemplate("/partials/infobar.html")->display($context);
        // line 7
        $this->env->loadTemplate("/partials/sections/bootstrap.html")->display($context);
    }

    public function getTemplateName()
    {
        return "pages/dashboard.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  41 => 7,  39 => 6,  36 => 5,  32 => 3,  29 => 2,);
    }
}
