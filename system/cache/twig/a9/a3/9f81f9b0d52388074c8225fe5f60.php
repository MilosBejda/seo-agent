<?php

/* /pages/dashboard.html */
class __TwigTemplate_a9a39f81f9b0d52388074c8225fe5f60 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("/layouts/left-sidebar.html");

        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "/layouts/left-sidebar.html";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_content($context, array $blocks = array())
    {
        // line 3
        $this->env->loadTemplate("/partials/infobar.html")->display($context);
        // line 4
        $this->env->loadTemplate("/pages/dashboard.html")->display($context);
        // line 5
        echo "





";
    }

    public function getTemplateName()
    {
        return "/pages/dashboard.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  35 => 5,  33 => 4,  31 => 3,  28 => 2,);
    }
}
