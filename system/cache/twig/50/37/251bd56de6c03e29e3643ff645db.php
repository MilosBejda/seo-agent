<?php

/* /partials/breadcrumbs.html */
class __TwigTemplate_5037251bd56de6c03e29e3643ff645db extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "
<div class=\"container-fluid padded\">
    <div class=\"row-fluid\">
<div id=\"breadcrumbs\">
        <div class=\"breadcrumb-button blue\">
          <span class=\"breadcrumb-label\"><i></i> <span data-bind=\"text:loaded.member().name\"></span></span>
       <!-- ko if: (seoData().websiteUrl != undefined) -->

       <span class=\"breadcrumb-arrow\"><span></span></span>

       <!-- /ko -->

       </div>

        
       <!-- ko if: (seoData().websiteUrl != undefined) -->

        <div class=\"breadcrumb-button\">
          <span class=\"breadcrumb-label\">
            <i></i> <span data-bind=\"text:seoData().websiteUrl\"></span>
          </span>
          <span class=\"breadcrumb-arrow\"><span></span></span>
        </div>
        <!-- /ko -->
        
      </div>

    </div>
  </div>





";
    }

    public function getTemplateName()
    {
        return "/partials/breadcrumbs.html";
    }

    public function getDebugInfo()
    {
        return array (  19 => 1,);
    }
}
