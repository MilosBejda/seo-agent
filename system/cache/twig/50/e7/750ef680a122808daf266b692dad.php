<?php

/* macros/resources.html */
class __TwigTemplate_50e7750ef680a122808daf266b692dad extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
    }

    // line 1
    public function getscript($_link = null)
    {
        $context = $this->env->mergeGlobals(array(
            "link" => $_link,
        ));

        $blocks = array();

        ob_start();
        try {
            // line 2
            echo "<script type=\"text/javascript\" src=\"";
            echo twig_escape_filter($this->env, base_js(), "html", null, true);
            if (isset($context["link"])) { $_link_ = $context["link"]; } else { $_link_ = null; }
            echo twig_escape_filter($this->env, $_link_, "html", null, true);
            echo "\"></script>
";
        } catch (Exception $e) {
            ob_end_clean();

            throw $e;
        }

        return ('' === $tmp = ob_get_clean()) ? '' : new Twig_Markup($tmp, $this->env->getCharset());
    }

    // line 4
    public function getbaseless_script($_link = null)
    {
        $context = $this->env->mergeGlobals(array(
            "link" => $_link,
        ));

        $blocks = array();

        ob_start();
        try {
            // line 5
            echo "<script type=\"text/javascript\" src=\"";
            if (isset($context["link"])) { $_link_ = $context["link"]; } else { $_link_ = null; }
            echo twig_escape_filter($this->env, $_link_, "html", null, true);
            echo "\"></script>
";
        } catch (Exception $e) {
            ob_end_clean();

            throw $e;
        }

        return ('' === $tmp = ob_get_clean()) ? '' : new Twig_Markup($tmp, $this->env->getCharset());
    }

    // line 7
    public function getstyle($_link = null, $_base = null, $_media = null)
    {
        $context = $this->env->mergeGlobals(array(
            "link" => $_link,
            "base" => $_base,
            "media" => $_media,
        ));

        $blocks = array();

        ob_start();
        try {
            // line 8
            echo "<link href=\"";
            echo twig_escape_filter($this->env, base_css(), "html", null, true);
            if (isset($context["link"])) { $_link_ = $context["link"]; } else { $_link_ = null; }
            echo twig_escape_filter($this->env, $_link_, "html", null, true);
            echo "\" media=\"";
            if (isset($context["media"])) { $_media_ = $context["media"]; } else { $_media_ = null; }
            echo twig_escape_filter($this->env, ((array_key_exists("media", $context)) ? (_twig_default_filter($_media_, "screen")) : ("screen")), "html", null, true);
            echo "\" rel=\"stylesheet\" type=\"text/css\" />
";
        } catch (Exception $e) {
            ob_end_clean();

            throw $e;
        }

        return ('' === $tmp = ob_get_clean()) ? '' : new Twig_Markup($tmp, $this->env->getCharset());
    }

    // line 10
    public function getbaseless_style($_link = null, $_media = null)
    {
        $context = $this->env->mergeGlobals(array(
            "link" => $_link,
            "media" => $_media,
        ));

        $blocks = array();

        ob_start();
        try {
            // line 11
            echo "<link href=\"";
            if (isset($context["link"])) { $_link_ = $context["link"]; } else { $_link_ = null; }
            echo twig_escape_filter($this->env, $_link_, "html", null, true);
            echo "\" media=\"";
            if (isset($context["media"])) { $_media_ = $context["media"]; } else { $_media_ = null; }
            echo twig_escape_filter($this->env, ((array_key_exists("media", $context)) ? (_twig_default_filter($_media_, "screen")) : ("screen")), "html", null, true);
            echo "\" rel=\"stylesheet\" type=\"text/css\" />
";
        } catch (Exception $e) {
            ob_end_clean();

            throw $e;
        }

        return ('' === $tmp = ob_get_clean()) ? '' : new Twig_Markup($tmp, $this->env->getCharset());
    }

    public function getTemplateName()
    {
        return "macros/resources.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  118 => 11,  106 => 10,  87 => 8,  74 => 7,  59 => 5,  48 => 4,  32 => 2,  21 => 1,);
    }
}
