<?php

/* resources/javascripts/footer-javascripts.html */
class __TwigTemplate_a56527fc6f7cf07565495a41a1c9095b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<script>
   var userProfile = ";
        // line 2
        if (isset($context["user"])) { $_user_ = $context["user"]; } else { $_user_ = null; }
        echo twig_jsonencode_filter($_user_);
        echo ";
</script>
<script   src=\"";
        // line 4
        echo twig_escape_filter($this->env, base_js(), "html", null, true);
        echo "/main.js?v=";
        echo twig_escape_filter($this->env, twig_random($this->env), "html", null, true);
        echo " \"></script>";
    }

    public function getTemplateName()
    {
        return "resources/javascripts/footer-javascripts.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  22 => 2,  92 => 19,  87 => 18,  82 => 17,  77 => 16,  72 => 15,  57 => 12,  52 => 11,  47 => 10,  37 => 8,  27 => 6,  25 => 5,  119 => 11,  107 => 10,  88 => 8,  75 => 7,  60 => 5,  32 => 7,  41 => 6,  19 => 1,  67 => 14,  62 => 13,  56 => 9,  54 => 18,  51 => 8,  49 => 4,  46 => 7,  44 => 12,  42 => 9,  40 => 10,  36 => 5,  33 => 7,  31 => 4,  28 => 4,  26 => 3,  21 => 1,);
    }
}
