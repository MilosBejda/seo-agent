<?php

/* pages/dashboard.html */
class __TwigTemplate_87618c05b8df1c51dede3e0cfa1a51a8 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("layouts/left-sidebar.html");

        $this->blocks = array(
            'sidebar' => array($this, 'block_sidebar'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "layouts/left-sidebar.html";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_sidebar($context, array $blocks = array())
    {
        // line 3
        $this->env->loadTemplate("sidebars/default-sidebar.html")->display($context);
    }

    // line 5
    public function block_content($context, array $blocks = array())
    {
        // line 6
        echo "<div class=\"container-fluid\">
<div class=\"row-fluid\">
</div>
</div>
<div class=\"container-fluid padded\">
<div class=\"row-fluid\">
</div> 
</div>


";
    }

    public function getTemplateName()
    {
        return "pages/dashboard.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  39 => 6,  36 => 5,  32 => 3,  29 => 2,);
    }
}
