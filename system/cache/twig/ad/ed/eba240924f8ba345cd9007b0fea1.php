<?php

/* /partials/progress.html */
class __TwigTemplate_adedeba240924f8ba345cd9007b0fea1 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"box\">
  <div class=\"box-header\">
    <div class=\"title\">SEO SpreadSheet</div>
    <ul class=\"box-toolbar\">
      <li class=\"toolbar-link\">
        <a href=\"#\" data-toggle=\"dropdown\"><i class=\"icon-cog\"></i></a>
        <ul class=\"dropdown-menu\">
          <li><a href=\"#\" data-bind=\"click: saveSpreadSheet\"><i class=\"icon-plus-sign\"></i> Save</a></li>
          <li><a href=\"#\"><i class=\"icon-remove-sign\"></i> Remove</a></li>
          <li><a href=\"#\"><i class=\"icon-pencil\"></i> Edit</a></li>
        </ul>
      </li>
    </ul>
  </div>


 <div id=\"results\" style=\"overflow: auto\"></div>


  
</div>";
    }

    public function getTemplateName()
    {
        return "/partials/progress.html";
    }

    public function getDebugInfo()
    {
        return array (  19 => 1,);
    }
}
