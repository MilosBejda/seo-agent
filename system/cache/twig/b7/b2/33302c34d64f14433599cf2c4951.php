<?php

/* resources/stylesheets/header-stylesheets.html */
class __TwigTemplate_b7b233302c34d64f14433599cf2c4951 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        $context["css"] = $this->env->loadTemplate("macros/resources.html");
        // line 2
        if (isset($context["css"])) { $_css_ = $context["css"]; } else { $_css_ = null; }
        echo $_css_->getstyle("/uploader/basic.css");
        echo "
";
        // line 3
        if (isset($context["css"])) { $_css_ = $context["css"]; } else { $_css_ = null; }
        echo $_css_->getstyle("/uploader/dropzone.css");
        echo "
";
        // line 4
        if (isset($context["css"])) { $_css_ = $context["css"]; } else { $_css_ = null; }
        echo $_css_->getstyle("/animate.css");
        echo "
";
        // line 5
        if (isset($context["css"])) { $_css_ = $context["css"]; } else { $_css_ = null; }
        echo $_css_->getbaseless_style("http://cdnjs.cloudflare.com/ajax/libs/font-awesome/3.0.0/css/font-awesome.min.css");
        echo "
";
        // line 6
        if (isset($context["css"])) { $_css_ = $context["css"]; } else { $_css_ = null; }
        echo $_css_->getbaseless_style("http://code.jquery.com/ui/1.10.2/themes/smoothness/jquery-ui.css");
        echo "
";
        // line 7
        if (isset($context["css"])) { $_css_ = $context["css"]; } else { $_css_ = null; }
        echo $_css_->getbaseless_style("http://weblab.alirezadesigner.com/projects/MetroPreLoader/metropreloader.css");
        echo "
";
        // line 8
        if (isset($context["css"])) { $_css_ = $context["css"]; } else { $_css_ = null; }
        echo $_css_->getbaseless_style("https://fonts.googleapis.com/css?family=Open+Sans:400,600,800");
        echo "
";
        // line 9
        if (isset($context["css"])) { $_css_ = $context["css"]; } else { $_css_ = null; }
        echo $_css_->getstyle("/application.css");
        echo "
<style>
    .dashboard-section {display:none;}
    .files ul li {display:inline; padding:10px;}
    .uploader {visibility : hidden !important}
 
table {width:100% !important}
.modal {width:600px;};
</style>
";
    }

    public function getTemplateName()
    {
        return "resources/stylesheets/header-stylesheets.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  56 => 9,  51 => 8,  46 => 7,  41 => 6,  36 => 5,  31 => 4,  26 => 3,  21 => 2,  19 => 1,);
    }
}
