<?php

/* partials/breadcrumbs.html */
class __TwigTemplate_d5f5b982a2017f4e4278a780c95d73bc extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div id=\"breadcrumbs\">

        <div class=\"breadcrumb-button blue\">
          <span class=\"breadcrumb-label\">
            <i data-bind=\"css: dashboard.smallIcon\"></i><span data-bind=\"text: dashboard.sectionName\"> </span></span>
          <span class=\"breadcrumb-arrow\"><span></span></span>
        </div>
    </div>
";
    }

    public function getTemplateName()
    {
        return "partials/breadcrumbs.html";
    }

    public function getDebugInfo()
    {
        return array (  19 => 1,);
    }
}
