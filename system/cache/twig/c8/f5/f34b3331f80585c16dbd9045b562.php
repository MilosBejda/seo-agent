<?php

/* /partials/chat.html */
class __TwigTemplate_c8f5f34b3331f80585c16dbd9045b562 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"box closable-chat-box\">
  <div class=\"box-content padded\">

    <div class=\"fields\">
      <div class=\"avatar\"><img class=\"avatar-small\" data-bind=\"attr : {src: userProfile.user().picture }\" /></div>
      <ul>
        <li><b>Add a comment for project <a href=\"#\">for SEO Agents</a></b></li>
        <li class=\"note\">Click on the textarea below to expand it!</li>
      </ul>
    </div>

    <form class=\"fill-up\" action=\"/\">

      <div class=\"chat-message-box\">
        <textarea name=\"ttt\" id=\"ttt\" rows=\"5\" placeholder=\"add a comment (click to expand!)\"></textarea>
      </div>

      <div class=\"clearfix actions\">
        <div class=\"pull-right faded-toolbar\">
          <a href=\"#\" class=\"btn btn-blue btn-mini send-comment\" data-bind=\"click: sendComment\">Send</a>
        </div>
      </div>
    </form>

  </div>
</div>";
    }

    public function getTemplateName()
    {
        return "/partials/chat.html";
    }

    public function getDebugInfo()
    {
        return array (  19 => 1,);
    }
}
