<?php

/* layouts/no-sidebar.html */
class __TwigTemplate_56d482c8c94f15e86631978b38216357 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!doctype html>
<html>
<head>
  <meta name=\"viewport\" content=\"width=device-width, maximum-scale=1, initial-scale=1, user-scalable=0\">
  <link rel=\"stylesheet\" href=\"https://fonts.googleapis.com/css?family=Open+Sans:400,600,800\">
   <meta charset=\"utf-8\">

  <!-- Always force latest IE rendering engine or request Chrome Frame -->
  <meta content=\"IE=edge,chrome=1\" http-equiv=\"X-UA-Compatible\">

  <!-- Use title if it's in the page YAML frontmatter -->
  <title>SEOAgent Login</title>

  <link href=\"";
        // line 14
        echo twig_escape_filter($this->env, base_css(), "html", null, true);
        echo "/application.css\" media=\"screen\" rel=\"stylesheet\" type=\"text/css\" />

  <!--[if lt IE 9]>
  <script src=\"../../javascripts/vendor/html5shiv.js\" type=\"text/javascript\"></script>
  <script src=\"../../javascripts/vendor/excanvas.js\" type=\"text/javascript\"></script>
  <![endif]-->

  <script src=\"";
        // line 21
        echo twig_escape_filter($this->env, base_js(), "html", null, true);
        echo "/application.js\" type=\"text/javascript\"></script>
</head>
<body>
";
        // line 24
        if (isset($context["user"])) { $_user_ = $context["user"]; } else { $_user_ = null; }
        if ($this->getAttribute($_user_, "is_loggedin")) {
            // line 25
            $this->env->loadTemplate("navigations/logged-navigation.html")->display($context);
        } else {
            // line 27
            $this->env->loadTemplate("navigations/default-navigation.html")->display($context);
        }
        // line 29
        echo "<div class=\"container\">
";
        // line 30
        $this->displayBlock('content', $context, $blocks);
        // line 32
        echo "</div>
</body>
</html>
";
    }

    // line 30
    public function block_content($context, array $blocks = array())
    {
    }

    public function getTemplateName()
    {
        return "layouts/no-sidebar.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  72 => 30,  65 => 32,  63 => 30,  60 => 29,  57 => 27,  54 => 25,  51 => 24,  45 => 21,  35 => 14,  20 => 1,  69 => 39,  31 => 3,  28 => 2,);
    }
}
