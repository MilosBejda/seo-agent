<?php

/* resources/meta/meta.html */
class __TwigTemplate_ee2cb89c742d111d72578feb84d360df extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<meta name=\"viewport\" content=\"width=device-width, maximum-scale=1, initial-scale=1, user-scalable=0\">
<meta charset=\"utf-8\">
<meta content=\"IE=edge,chrome=1\" http-equiv=\"X-UA-Compatible\">";
    }

    public function getTemplateName()
    {
        return "resources/meta/meta.html";
    }

    public function getDebugInfo()
    {
        return array (  19 => 1,  67 => 15,  62 => 12,  56 => 19,  54 => 18,  51 => 17,  49 => 15,  46 => 14,  44 => 12,  42 => 11,  40 => 10,  36 => 8,  33 => 7,  31 => 6,  28 => 5,  26 => 4,  21 => 1,);
    }
}
