<?php

/* /partials/sections.html */
class __TwigTemplate_0d93f9dc956e36d645cee47d8796c34f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"container-fluid\">
<div class=\"row-fluid\">
";
        // line 3
        $this->env->loadTemplate("/partials/breadcrumbs.html")->display($context);
        // line 4
        echo "
</div>
</div>
<div class=\"container-fluid padded\">

<div id=\"dashboard\" class=\"row-fluid dashboard-section\">
";
        // line 10
        $this->env->loadTemplate("/partials/chat-timeline.html")->display($context);
        // line 11
        $this->env->loadTemplate("/partials/chat.html")->display($context);
        // line 12
        echo "</div> 

<div  id=\"progress\" class=\"row-fluid dashboard-section\">
";
        // line 15
        $this->env->loadTemplate("/partials/sections/progress.html")->display($context);
        // line 16
        echo "
    
</div>
<div  id=\"reports\" class=\"row-fluid dashboard-section\">
";
        // line 20
        $this->env->loadTemplate("/partials/sections/reports.html")->display($context);
        // line 21
        echo "    
</div>
<div  id=\"administration\" class=\"row-fluid dashboard-section\">
    
</div>
</div> 
";
        // line 27
        $this->env->loadTemplate("/partials/paymentModal.html")->display($context);
        // line 28
        echo "


";
    }

    public function getTemplateName()
    {
        return "/partials/sections.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  62 => 28,  60 => 27,  52 => 21,  50 => 20,  42 => 15,  37 => 12,  35 => 11,  33 => 10,  25 => 4,  23 => 3,  19 => 1,  187 => 88,  184 => 87,  179 => 83,  176 => 82,  164 => 103,  154 => 97,  148 => 94,  143 => 92,  139 => 90,  137 => 87,  133 => 85,  131 => 82,  126 => 79,  124 => 78,  103 => 60,  98 => 58,  85 => 48,  79 => 45,  65 => 34,  48 => 20,  44 => 16,  40 => 18,  21 => 1,);
    }
}
