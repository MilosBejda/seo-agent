<?php

/* pages/login.html */
class __TwigTemplate_c2629e941eb75b3ce1173a6d38ca17d2 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("layouts/no-sidebar.html");

        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "layouts/no-sidebar.html";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_content($context, array $blocks = array())
    {
        // line 3
        echo "<div class=\"span4 offset4\">
  <div class=\"padded\">
    <div class=\"login box\" style=\"margin-top: 80px;\">

      <div class=\"box-header\">
        <span class=\"title\">Login With Google</span>
      </div>

      <div class=\"box-content padded\">
        <form class=\"separate-sections\">

     
        <a class=\"btn btn-blue btn-block\" href=\"";
        // line 15
        if (isset($context["loginUrl"])) { $_loginUrl_ = $context["loginUrl"]; } else { $_loginUrl_ = null; }
        echo twig_escape_filter($this->env, $_loginUrl_, "html", null, true);
        echo "\">
                Login <i class=\"icon-signin\"></i>
            </a>

        </form>

        <div>
      
        </div>
      </div>

    </div>

  
  </div>
</div>
";
    }

    public function getTemplateName()
    {
        return "pages/login.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  45 => 15,  31 => 3,  28 => 2,);
    }
}
