<?php

/* /partials/chat-timeline.html */
class __TwigTemplate_890b1fa88d715159ca30151254a2850c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<ul class=\"chat-box timeline\" data-bind=\"foreach: {data: comments, afterAdd:slideDown, beforeRemove: slideUp}\">
  <li class=\"arrow-box-left gray\">
    <div class=\"avatar\"><img class=\"avatar-small\" data-bind=\"attr : {src: picture}\" /></div>
    <div class=\"info\">
      <span class=\"name\">
        <span class=\"label label-green\">COMMENT</span> <strong class=\"indent\"><span data-bind=\"text: name\"></span></strong>
      </span>
    </div>
    <div class=\"content\">
      <blockquote data-bind=\"html: message\">

          
          
      </blockquote>
 
    </div>
  </li>

</ul>";
    }

    public function getTemplateName()
    {
        return "/partials/chat-timeline.html";
    }

    public function getDebugInfo()
    {
        return array (  19 => 1,);
    }
}
