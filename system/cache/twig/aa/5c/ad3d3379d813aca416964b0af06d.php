<?php

/* /layouts/left-sidebar.html */
class __TwigTemplate_aa5cad3d3379d813aca416964b0af06d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!doctype html>
<html>
<head>

  <meta name=\"viewport\" content=\"width=device-width, maximum-scale=1, initial-scale=1, user-scalable=0\">
  <link rel=\"stylesheet\" href=\"https://fonts.googleapis.com/css?family=Open+Sans:400,600,800\">


  <meta charset=\"utf-8\">

  <!-- Always force latest IE rendering engine or request Chrome Frame -->
  <meta content=\"IE=edge,chrome=1\" http-equiv=\"X-UA-Compatible\">

  <!-- Use title if it's in the page YAML frontmatter -->
  <title>Core Admin Theme</title>

  <link href=\"";
        // line 17
        echo twig_escape_filter($this->env, base_css(), "html", null, true);
        echo "/application.css\" media=\"screen\" rel=\"stylesheet\" type=\"text/css\" />
    <link href=\"";
        // line 18
        echo twig_escape_filter($this->env, base_css(), "html", null, true);
        echo "/uploader/basic.css\" media=\"screen\" rel=\"stylesheet\" type=\"text/css\" />
    <link href=\"";
        // line 19
        echo twig_escape_filter($this->env, base_css(), "html", null, true);
        echo "/uploader/dropzone.css\" media=\"screen\" rel=\"stylesheet\" type=\"text/css\" />

   <link href=\"http://cdnjs.cloudflare.com/ajax/libs/font-awesome/3.0.0/css/font-awesome.min.css\" media=\"screen\" rel=\"stylesheet\" type=\"text/css\" />
 

  <!--[if lt IE 9]>
  <script src=\"../../javascripts/vendor/html5shiv.js\" type=\"text/javascript\"></script>
  <script src=\"../../javascripts/vendor/excanvas.js\" type=\"text/javascript\"></script>
  <![endif]-->

  <script src=\"";
        // line 29
        echo twig_escape_filter($this->env, base_js(), "html", null, true);
        echo "/application.js\" type=\"text/javascript\"></script>

 <script src=\" http://ajax.aspnetcdn.com/ajax/knockout/knockout-2.2.1.js\" type=\"text/javascript\"></script>
   <script src=\"https://raw.github.com/SteveSanderson/knockout.mapping/master/knockout.mapping.js\" type=\"text/javascript\"></script>

<script src=\"http://code.highcharts.com/highcharts.js\"></script>
<script src=\"http://code.highcharts.com/modules/exporting.js\"></script>

<script src=\"";
        // line 37
        echo twig_escape_filter($this->env, base_js(), "html", null, true);
        echo "/cookie.js\"></script>
  <script src=\"http://js.pusher.com/2.0/pusher.min.js\" type=\"text/javascript\"></script>
<style>
    .dashboard-section {display:none;}
    .files ul li {display:inline; padding:10px;}
    .uploader {visibility : hidden !important}
    
</style>
<script src=\"";
        // line 45
        echo twig_escape_filter($this->env, base_js(), "html", null, true);
        echo "/uploader/dropzone.min.js\"></script>

</head>
<body>
   <script type=\"text/javascript\">
      (function() {
       var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
       po.src = 'https://apis.google.com/js/client:plusone.js';
       var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
     })();
                                                                                                                                                                      

    </script>

";
        // line 59
        if (isset($context["user"])) { $_user_ = $context["user"]; } else { $_user_ = null; }
        if ($this->getAttribute($_user_, "is_loggedin")) {
            // line 60
            $this->env->loadTemplate("navigations/loggedin-navigation.html")->display($context);
        } else {
            // line 62
            $this->env->loadTemplate("navigations/default-navigation.html")->display($context);
        }
        // line 64
        echo "
";
        // line 65
        $this->env->loadTemplate("partials/notifications.html")->display($context);
        // line 66
        echo "


<div class=\"main-content\">
";
        // line 70
        $this->displayBlock('content', $context, $blocks);
        // line 73
        echo "  <script src=\"http://js.pusher.com/2.0/pusher.min.js\" type=\"text/javascript\"></script>

  
<script src=\"";
        // line 76
        echo twig_escape_filter($this->env, base_js(), "html", null, true);
        echo "/PusherNotifier.js\"></script>
<script>

   var userProfile = ";
        // line 79
        if (isset($context["user"])) { $_user_ = $context["user"]; } else { $_user_ = null; }
        echo twig_jsonencode_filter($_user_);
        echo ";


  

</script>
<script   src=\"";
        // line 85
        echo twig_escape_filter($this->env, base_js(), "html", null, true);
        echo "/main.js?v=";
        echo twig_escape_filter($this->env, twig_random($this->env), "html", null, true);
        echo " \"></script>

<script>
</script>
</div>
</body>
</html>
";
    }

    // line 70
    public function block_content($context, array $blocks = array())
    {
        // line 71
        echo "
";
    }

    public function getTemplateName()
    {
        return "/layouts/left-sidebar.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  158 => 71,  155 => 70,  141 => 85,  131 => 79,  125 => 76,  120 => 73,  118 => 70,  112 => 66,  110 => 65,  107 => 64,  104 => 62,  101 => 60,  98 => 59,  81 => 45,  70 => 37,  59 => 29,  46 => 19,  42 => 18,  38 => 17,  20 => 1,);
    }
}
