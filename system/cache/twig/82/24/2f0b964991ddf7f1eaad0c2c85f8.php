<?php

/* navigations/default-navigation.html */
class __TwigTemplate_82242f0b964991ddf7f1eaad0c2c85f8 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"navbar navbar-top navbar-inverse\">
  <div class=\"navbar-inner\">
    <div class=\"container-fluid\">

      <a class=\"brand\" href=\"#\">SEO Agent</a>

      <!-- the new toggle buttons -->

      <ul class=\"nav pull-right\">

        <li class=\"toggle-primary-sidebar hidden-desktop\" data-toggle=\"collapse\" data-target=\".nav-collapse-primary\"><a><i class=\"icon-th-list\"></i></a></li>

        <li class=\"collapsed hidden-desktop\" data-toggle=\"collapse\" data-target=\".nav-collapse-top\"><a><i class=\"icon-align-justify\"></i></a></li>

      </ul>

      <ul class=\"nav pull-right\">
              <li class=\"active\"><a href=\"#\" title=\"Go home\"><i class=\"icon-home\"></i> Home</a></li>             
    
                  <li class=\"dropdown\">
                <a href=\"#\" class=\"dropdown-toggle\" data-toggle=\"dropdown\"><i class=\"icon-user\"></i><span ></span> <b class=\"caret\"></b></a>
                <ul class=\"dropdown-menu\">
                  <li><a href=\"#\"> <span data-bind=\"text: name\"> </span></a></li>
                                   <li><a href=\"#\"> <span data-bind=\"text: name2\"> </span></a></li>
 
             
                </ul>
              </li>
              
     

            </ul>
          

          <div class=\"nav-collapse nav-collapse-top\">
            <ul class=\"nav pull-right\">
              <li><a href=\"#\" title=\"Manage users\">
                  
                  
  
                  
              </a></li>
            </ul>

          </div>
      

    </div>
  </div>
</div>
              ";
    }

    public function getTemplateName()
    {
        return "navigations/default-navigation.html";
    }

    public function getDebugInfo()
    {
        return array (  19 => 1,);
    }
}
