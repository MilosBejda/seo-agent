<?php

/* sidebars/default-sidebar.html */
class __TwigTemplate_53efce6b572f61548f481ac94e9f523c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "
<div class=\"sidebar-background\">
  <div class=\"primary-sidebar-background\"></div>
</div>

<div id=\"sidebar\" class=\"primary-sidebar\">

  <!-- Main nav -->
  <ul class=\"nav nav-collapse collapse nav-collapse-primary\">


            <li class=\"active\">
              <span class=\"glow\"></span>
                       <a href=\"#dashboard1\" data-bind=\"click: loadDashboardSection\">

                  <i class=\"icon-dashboard icon-2x\"></i>
                  <span>Dashboard</span>
              </a>
            </li>



  <li class=\"\">
              <span class=\"glow\"></span>
              <a href=\"#progress\" data-bind=\"click: loadDashboardSection\">
                  <i class=\"icon-bar-chart icon-2x\"></i>
                  <span>Progress</span>
              </a>
            </li>

            <li class=\"\">
              <span class=\"glow\"></span>
              <a href=\"#reports\" data-bind=\"click: loadDashboardSection\">
                  <i class=\"icon-edit icon-2x\"></i>
                  <span>Reports</span>
              </a>
            </li>

        

    

        

          
        

    

        

            <li class=\"dark-nav \">

              <span class=\"glow\"></span>

              

              <a class=\"accordion-toggle collapsed \" data-toggle=\"collapse\" href=\"#wW7abHF3rv\">
                  <i class=\"icon-beaker icon-2x\"></i>
                    <span>
                Administrator
                      <i class=\"icon-caret-down\"></i>
                    </span>

              </a>

              <ul id=\"wW7abHF3rv\" class=\"collapse \">
                
                    <li class=\"\">
                      <a data-bind=\"click: loadDashboardSection\">
                          <i class=\"icon-hand-up\"></i> My Reports
                      </a>
                    </li>
                
                    <li class=\"\">
                      <a data-bind=\"click: loadDashboardSection\">
                          <i class=\"icon-beaker\"></i> Progress
                      </a>
                    </li>
                
                    <li class=\"\">
                      <a data-bind=\"click: loadDashboardSection\">
                          <i class=\"icon-info-sign\"></i> Users
                      </a>
                    </li>
                
                    <li class=\"\">
                      <a data-bind=\"click: loadDashboardSection\">
                          <i class=\"icon-th-large\"></i> Task Lists
                      </a>
                    </li>
                
                    <li class=\"\">
                      <a data-bind=\"click: loadDashboardSection\">
                          <i class=\"icon-table\"></i> Messages
                      </a>
                    </li>
                
                    <li class=\"\">
                      <a data-bind=\"click: loadDashboardSection\">
                          <i class=\"icon-plus-sign-alt\"></i> Database
                      </a>
                    </li>
                
              </ul>

            </li>

        

    

        


        

    

        

            <li class=\"dark-nav \">

              <span class=\"glow\"></span>

              

              <a class=\"accordion-toggle collapsed \" data-toggle=\"collapse\" href=\"#PVhCCMs6NX\">
                  <i class=\"icon-link icon-2x\"></i>
                    <span>
                      Others
                      <i class=\"icon-caret-down\"></i>
                    </span>

              </a>

              <ul id=\"PVhCCMs6NX\" class=\"collapse \">
                
                    <li class=\"\">
                      <a href=\"../other/wizard.html\">
                          <i class=\"icon-magic\"></i> Wizard
                      </a>
                    </li>
                
                    <li class=\"\">
                      <a href=\"../other/login.html\">
                          <i class=\"icon-user\"></i> Login Page
                      </a>
                    </li>
                
                    <li class=\"\">
                      <a href=\"../other/sign_up.html\">
                          <i class=\"icon-user\"></i> Sign Up Page
                      </a>
                    </li>
                
                    <li class=\"\">
                      <a href=\"../other/full_calendar.html\">
                          <i class=\"icon-calendar\"></i> Full Calendar
                      </a>
                    </li>
                
                    <li class=\"\">
                      <a href=\"../other/error404.html\">
                          <i class=\"icon-ban-circle\"></i> Error 404 page
                      </a>
                    </li>
                
              </ul>

            </li>

        

    

  </ul>

  <div class=\"hidden-tablet hidden-phone\">
  
    <div class=\"text-center\" style=\"margin-top: 60px\">
      <div class=\"easy-pie-chart-percent\" style=\"display: inline-block\" data-percent=\"89\"><span>89%</span></div>
      <div style=\"padding-top: 20px\"><b>Monthly Progress</b></div>
    </div>

    <hr class=\"divider\" style=\"margin-top: 60px\">

    <div class=\"sparkline-box side\">

      <div class=\"sparkline-row\">
        <h4 class=\"gray\"><span>Orders</span> 847</h4>
        <div class=\"sparkline big\" data-color=\"gray\"><!--8,5,6,14,4,11,6,29,26,29,8,7--></div>
      </div>

      <hr class=\"divider\">
      <div class=\"sparkline-row\">
        <h4 class=\"dark-green\"><span>Income</span> \$43.330</h4>
        <div class=\"sparkline big\" data-color=\"darkGreen\"><!--12,22,28,21,20,23,5,20,22,24,23,7--></div>
      </div>

      <hr class=\"divider\">
      <div class=\"sparkline-row\">
        <h4 class=\"blue\"><span>Reviews</span> 223</h4>
        <div class=\"sparkline big\" data-color=\"blue\"><!--3,19,5,24,11,8,8,29,5,14,26,10--></div>
      </div>

      <hr class=\"divider\">
    </div>
  </div>
 </div>
";
    }

    public function getTemplateName()
    {
        return "sidebars/default-sidebar.html";
    }

    public function getDebugInfo()
    {
        return array (  19 => 1,  53 => 14,  51 => 13,  45 => 9,  43 => 8,  39 => 6,  36 => 5,  32 => 3,  29 => 2,);
    }
}
