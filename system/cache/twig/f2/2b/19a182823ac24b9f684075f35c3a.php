<?php

/* /partials/paymentModal.html */
class __TwigTemplate_f22b19a182823ac24b9f684075f35c3a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"wizard\" id=\"some-wizard\">
 
    <h1>Budget Management </h1>
 
    <div class=\"wizard-card\" data-cardname=\"card0\">
        <h3>Create A Budget</h3>


<input type=\"text\" name=\"budgetAmount\" type=\"text\">
<form action=\"\" method=\"POST\" id=\"payment-form\">
  <span class=\"payment-errors\"></span>

  <div class=\"form-row\">
    <label>
      <span>Card Number</span>
      <input type=\"text\" size=\"20\" data-stripe=\"number\" value=\"4242424242424242\"/>
    </label>
  </div>

  <div class=\"form-row\">
    <label>
      <span>CVC</span>
      <input type=\"text\" size=\"4\" data-stripe=\"cvc\" value=\"123\"/>
    </label>
  </div>

  <div class=\"form-row\">
    <label>
      <span>Expiration (MM/YYYY)</span>
      <input type=\"text\" size=\"2\" data-stripe=\"exp-month\" value=\"11\"/>
    </label>
    <span> / </span>
    <input type=\"text\" size=\"4\" data-stripe=\"exp-year\" value=\"14\"/>
  </div>

 <a class=\"btn btn-primary\" data-bind=\"click: addPaymentInformation\">Submit</a>

</form>

  <a class=\"btn btn-primary\" data-bind=\"click: setBudgetPlan\">Set Plan</a>


    </div>
 
    <div class=\"wizard-card\" data-cardname=\"card2\">
        <h3>Allocate Budget To Services</h3>
        Some other content
    </div>
 
</div>
















<div id=\"static\" class=\"modal hide fade \" tabindex=\"-1\" data-backdrop=\"static\" data-keyboard=\"false\">
  <div class=\"modal-body\">
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
<div class=\"row\">
    <div class=\"span6\">
\t\t
\t\t<table class=\"table table-condensed table-hover\">
\t\t\t<thead>
\t\t\t\t<tr>
\t\t\t\t\t<th> </th>
\t\t\t\t\t<th>Level 1</th>
\t\t\t\t\t<th>Level 2</th>
\t\t\t\t\t<th>Level 3</th>
\t\t\t\t</tr>
\t\t\t</thead>
\t\t\t<tbody>
\t\t\t\t<tr>
\t\t\t\t\t<td>Link Building</td>
\t\t\t\t\t<td>1</td>
\t\t\t\t\t<td>2</td>
\t\t\t\t\t<td>3</td>
\t\t\t\t</tr>
\t\t\t\t<tr>
\t\t\t\t\t<td>Directory Management</td>
\t\t\t\t\t<td>1</td>
\t\t\t\t\t<td>2</td>
\t\t\t\t\t<td>3</td>
\t\t\t\t</tr>
\t\t\t\t<tr>
\t\t\t\t\t<td>Article Management</td>
\t\t\t\t\t<td>1</td>
\t\t\t\t\t<td>2</td>
\t\t\t\t\t<td>4</td>
\t\t\t\t</tr>
\t\t\t\t<tr>
\t\t\t\t\t<td>Onsite Management</td>
\t\t\t\t\t<td>1 site</td>
\t\t\t\t\t<td>2 sites</td>
\t\t\t\t\t<td>1 site</td>
\t\t\t\t</tr>
\t\t\t\t<tr>
\t\t\t\t\t<td>Support</td>
\t\t\t\t\t<td>2 hours</td>
\t\t\t\t\t<td>48 hours</td>
\t\t\t\t\t<td>Monthly</td>
\t\t\t\t</tr>
\t\t
\t\t\t\t<tr>
\t\t\t\t\t<td> </td>
\t\t\t\t\t<td><a class=\"btn btn-success\" href=\"#buy\" data-toggle=\"modal\"><i class=\"icon-shopping-cart icon-white\"></i> Order »</a></td>
\t\t\t\t\t<td><a class=\"btn btn-success\" href=\"#buy\" data-toggle=\"modal\"><i class=\"icon-shopping-cart icon-white\"></i> Order »</a></td>
\t\t\t\t\t<td><a class=\"btn btn-success\" href=\"#buy\" data-toggle=\"modal\"><i class=\"icon-shopping-cart icon-white\"></i> Order »</a></td>
\t\t\t\t</tr>
\t\t\t</tbody>
\t\t</table>
 
\t</div>
</div>
   
   
   
   
   
   
   
   
   
   
   
   
   
   
  </div>
  <div class=\"modal-footer\">
    <button type=\"button\" data-dismiss=\"modal\" class=\"btn\">Cancel</button>
    <button type=\"button\" data-dismiss=\"modal\" class=\"btn btn-primary\">Continue Task</button>
  </div>
</div>





<div id=\"buy\" class=\"modal hide fade\" tabindex=\"-1\" data-focus-on=\"input:first\">
  <div class=\"modal-header\">
    <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\">×</button>
    <h3>Stack Two</h3>
  </div>
  <div class=\"modal-body\">
    <p>One fine body…</p>
    <p>One fine body…</p>
    <input type=\"text\" data-tabindex=\"1\">
    <input type=\"text\" data-tabindex=\"2\">
    <button class=\"btn\" data-toggle=\"modal\" href=\"#stack3\">Launch modal</button>
  </div>
  <div class=\"modal-footer\">
    <button type=\"button\" data-dismiss=\"modal\" class=\"btn\">Close</button>
    <button type=\"button\" class=\"btn btn-primary\">Ok</button>
  </div>
</div>";
    }

    public function getTemplateName()
    {
        return "/partials/paymentModal.html";
    }

    public function getDebugInfo()
    {
        return array (  19 => 1,);
    }
}
