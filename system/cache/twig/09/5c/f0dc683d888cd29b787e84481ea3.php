<?php

/* resources/javascripts/body-javascripts.html */
class __TwigTemplate_095cf0dc683d888cd29b787e84481ea3 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<script type=\"text/javascript\">
      (function() {
       var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
       po.src = 'https://apis.google.com/js/client:plusone.js';
       var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
     })();                                                                                                                                                              
</script>";
    }

    public function getTemplateName()
    {
        return "resources/javascripts/body-javascripts.html";
    }

    public function getDebugInfo()
    {
        return array (  55 => 20,  51 => 19,  44 => 15,  40 => 14,  35 => 12,  28 => 8,  19 => 1,);
    }
}
