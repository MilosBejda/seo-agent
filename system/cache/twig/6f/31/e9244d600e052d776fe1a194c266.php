<?php

/* /partials/reports.html */
class __TwigTemplate_6f31e9244d600e052d776fe1a194c266 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"files\">
<h1>Reports</h1>
<form id=\"zone\" class=\"dropzone\" action=\"/uploader/\" method=\"post\" enctype=\"multipart/form-data\">
  <input type=\"file\" name=\"file\" />
</form>
    
</div>
";
    }

    public function getTemplateName()
    {
        return "/partials/reports.html";
    }

    public function getDebugInfo()
    {
        return array (  19 => 1,);
    }
}
